program Dilgu;

{$MODE Delphi}

{$R Xtra.dres}

uses
  Forms, Interfaces,
  MainFormU in 'MainFormU.pas' {MainForm},
  DocFormU in 'DocFormU.pas' {DocForm};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := '';
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TDocForm, DocForm);
  Application.Run;
end.
