unit MainFormU;

{$MODE Delphi}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ImgList, ShellAPI, process, Menus, Types, GraphUtil,
  ExtCtrls, FileUtil, Buttons;

type

  TMergeApplicationType = (maUnknown, maEXE, maDLL);

  { TMainForm }

  TMainForm = class(TForm)
    chkCompressOnly: TCheckBox;
    chkCompress: TCheckBox;
    chkCreateBackupFile: TCheckBox;
    chkDontCompressResources: TCheckBox;
    chkDontPatchStrongName: TCheckBox;
    chkForceCompression: TCheckBox;
    grpOptions: TGroupBox;
    MpressProcess: TProcess;
    IlMergeProcess: TProcess;
    txtSourceExe: TEdit;
    lblSourceExe: TLabel;
    btnMerge: TBitBtn;
    btnOpen: TBitBtn;
    btnClose: TBitBtn;
    il1: TImageList;
    dlgOpen: TOpenDialog;
    txtMergedExe: TEdit;
    lblMergedExe: TLabel;
    btnMergedExe: TBitBtn;
    grpMergeLog: TGroupBox;
    lstLog: TListBox;
    dlgSave: TSaveDialog;
    lstOtherAssemblies: TListBox;
    lblOtherAssemblies: TLabel;
    btnAddAssembliess: TBitBtn;
    pmOtherAssmblies: TPopupMenu;
    Remve1: TMenuItem;
    btnAbout: TBitBtn;
    pbTitle: TPaintBox;
    bvl1: TBevel;
    chkAdditionalCommands: TCheckBox;
    txtAdditionalCommands: TEdit;
    btnAdditionalCommands: TBitBtn;
    procedure btnCloseClick(Sender: TObject);
    procedure btnMergeClick(Sender: TObject);
    procedure btnOpenClick(Sender: TObject);
    procedure btnMergedExeClick(Sender: TObject);
    procedure btnAddAssembliessClick(Sender: TObject);
    procedure bvl1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure bvl1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure bvl1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure chkCompressOnlyChange(Sender: TObject);
    procedure chkCompressChange(Sender: TObject);
    procedure FormDropFiles(Sender: TObject; const FileNames: array of String);
    procedure Remve1Click(Sender: TObject);
    procedure btnAboutClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure pbTitlePaint(Sender: TObject);
    procedure chkAdditionalCommandsClick(Sender: TObject);
    procedure btnAdditionalCommandsClick(Sender: TObject);
    procedure txtSourceExeChange(Sender: TObject);
  private
    AppType: TMergeApplicationType;
    procedure ExecuteMerge;
    procedure ExecuteMpress;
    procedure SetAppTypeAndOutputName;
    procedure UpdateGUIControls(Enable: boolean);
    procedure DrawGradient;
    procedure SearchDir(Path: string);
  protected
    procedure WMDropFiles(var Message: TWMDropFiles); message WM_DROPFILES;
    procedure WMNCHitTest(var Message: TWMNCHitTest); message WM_NCHITTEST;
  public

  end;

const
  CRLF = #13#10;
  ApplicationName = 'Dilgum';
  ApplicationVersion = '2.1.3';
  ILMergeHelp =
    '[/log[:filename]] ' + CRLF + '[/keyfile:filename ' + CRLF +
    '[/delaysign]]' + CRLF + '[/internalize[:filename]]' + CRLF +
    '[/closed]' + CRLF + '[/ndebug]' + CRLF + '[/ver:version]' +
    CRLF + '[/copyattrs [/allowMultiple]]' + CRLF + '[/xmldocs]' +
    CRLF + '[/attr:filename]' + CRLF +
    '([/targetplatform:<version>[,<platformdir>]]|v1|v1.1|v2)' +
    CRLF + '[/useFullPublicKeyForReferences]' + CRLF + '[/zeroPeKind] ' +
    CRLF + '[/allowDup[:typename]]*' + CRLF + '[/allowDuplicateResources]' +
    CRLF + '[/union]' + CRLF + '[/align:n]';

var
  MainForm: TMainForm;
  ILMergePath: string; //set on Create

implementation

uses DocFormU;

{$R *.lfm}

{ TMainForm }

{
procedure ShellExecute_AndWait(FileName: string; Params: string);
var
  exInfo: TShellExecuteInfo;
  Ph: DWORD;
  LastItem:Integer;
begin
  MainForm.lstLog.Items.Add('');
  LastItem := MainForm.lstLog.Items.Count -1 ;

  FillChar(exInfo, SizeOf(exInfo), 0);
  with exInfo do
  begin
    cbSize := SizeOf(exInfo);
    fMask := SEE_MASK_NOCLOSEPROCESS or SEE_MASK_FLAG_DDEWAIT;
    Wnd := GetActiveWindow();
    ExInfo.lpVerb := 'open';
    ExInfo.lpParameters := PChar(Params);
    lpFile := PChar(FileName);
    nShow := SW_HIDE;
  end;
  if ShellExecuteEx(@exInfo) then
    Ph := exInfo.HProcess
  else
  begin
    ShowMessage(SysErrorMessage(GetLastError));
    Exit;
  end;
  while WaitForSingleObject(ExInfo.hProcess, 50) <> WAIT_OBJECT_0 do
  begin
    Application.ProcessMessages;
    MainForm.lstLog.Items[LastItem]:= MainForm.lstLog.Items[LastItem]+'*';
  end;
  FileClose(Ph); // *Converted from CloseHandle*
end;
}

procedure TMainForm.btnAddAssembliessClick(Sender: TObject);
begin
  dlgOpen.Options := dlgOpen.Options + [ofAllowMultiSelect];
  if dlgOpen.Execute then
  begin
    lstOtherAssemblies.Items.AddStrings(dlgOpen.Files);
  end;
  dlgOpen.Options := dlgOpen.Options - [ofAllowMultiSelect];
end;

var
  inReposition : boolean;
  oldPos : TPoint;
  ControlPressed : boolean;
procedure TMainForm.bvl1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
begin
  Self.Invalidate;
  if Button = mbLeft then
  begin
    inReposition := True;
    SetCapture(Self.Handle);
    GetCursorPos(oldPos);
    ControlPressed := True;
  end;

end;

procedure TMainForm.bvl1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  newPos: TPoint;
  frmPoint : TPoint;
begin
  if inReposition then
  begin
      GetCursorPos(newPos);
      Screen.Cursor := crSize;
      Left := Left - oldPos.X + newPos.X;
      Top := Top - oldPos.Y + newPos.Y;
      oldPos := newPos;
  end;
end;

procedure TMainForm.bvl1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if inReposition then
  begin
    Screen.Cursor := crDefault;
    ReleaseCapture;
    inReposition := False;
  end;
end;

procedure TMainForm.chkCompressOnlyChange(Sender: TObject);
begin
  if chkCompressOnly.Checked then
  begin
    btnMerge.Caption:='Comperss';
    chkCompress.Checked:=True;
    chkCompress.Enabled:=False;
  end else
  begin
    btnMerge.Caption:='Merge';
    chkCompress.Enabled:=True;
  end;
end;

procedure TMainForm.chkCompressChange(Sender: TObject);
begin
  grpOptions.Enabled:=chkCompress.Checked;
end;

procedure TMainForm.FormDropFiles(Sender: TObject;
  const FileNames: array of String);
begin
  if Length(FileNames)>0 then
  begin
    txtSourceExe.Text:=FileNames[0];
    SetAppTypeAndOutputName;
    SearchDir(ExtractFilePath(txtSourceExe.Text));
  end;
end;

procedure TMainForm.btnAdditionalCommandsClick(Sender: TObject);
begin
  if MessageDlg('Here is a list of commands to be used as additional commands' +
    CRLF + CRLF + ILMergeHelp + CRLF + CRLF +
    'Anyway...,Would you like to open documentation of ILMerge and see their meaning?',
    mtConfirmation, mbYesNo, 0) = mrYes then
    DocForm.ShowModal;
end;

procedure TMainForm.txtSourceExeChange(Sender: TObject);
begin
  if LowerCase(ExtractFileExt(txtSourceExe.Text))<>'.exe' then
  begin
    chkCompressOnly.Checked:=False;
    chkCompress.Checked:=False;
  end;
end;

procedure TMainForm.btnAboutClick(Sender: TObject);
var
  f: TForm;
  img: TImage;
begin
  try
    f := CreateMessageDialog(ApplicationName + ' ' + ApplicationVersion +
      CRLF + '=============================================' + CRLF +
      ApplicationName + ' is a GUI for Microsoft''s ILMerge and Mpress Compressor' + CRLF +
      'ILMerge is an application to merge multiple .NET Assemblies into single one' +CRLF+
      'Mpress is an application to compress .NET applications by MATCODE Software' + CRLF +
      CRLF + ApplicationName + ' features: ' + CRLF +
      ' 1. Supports drag and drop (source assembly)' + CRLF +
      ' 2. Automatically searches source''s dir for dll''s and adds them to list' +
      CRLF + '=============================================' + CRLF +
      'Flakron Shkodra 2011', mtInformation, [mbOK]);

    img := TImage.Create(f);
    img.Picture.Assign(Application.Icon);
    img.Left := 10;
    img.Top := 10;
    f.InsertControl(img);
    f.ShowModal;
  finally
    f.Free;
  end;

end;

procedure TMainForm.btnCloseClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TMainForm.btnMergeClick(Sender: TObject);
begin
  lstLog.Clear;
  UpdateGUIControls(False);
  try
    if not chkCompressOnly.Checked then
    ExecuteMerge;
    if chkCompress.Checked then
    ExecuteMpress;
  finally
    UpdateGUIControls(True);
  end;

end;

procedure TMainForm.btnMergedExeClick(Sender: TObject);
begin
  if dlgSave.Execute then
  begin
    txtMergedExe.Text := dlgSave.FileName;
  end;
end;

procedure TMainForm.btnOpenClick(Sender: TObject);

begin
  if dlgOpen.Execute then
  begin
    if FileExistsUTF8(dlgOpen.FileName) { *Converted from FileExists*  } then
    begin

      txtSourceExe.Text := dlgOpen.FileName;
      SetAppTypeAndOutputName;
      SearchDir(ExtractFilePath(txtSourceExe.Text));
    end;
  end;
end;

procedure TMainForm.chkAdditionalCommandsClick(Sender: TObject);
begin
  txtAdditionalCommands.Enabled := chkAdditionalCommands.Checked;
  btnAdditionalCommands.Enabled := chkAdditionalCommands.Checked;
end;

procedure TMainForm.DrawGradient;
begin

end;

procedure TMainForm.ExecuteMerge;
var
  ilMergerParams: string;
  ilMergeLog: string;
  assemblies: string;
  mergeType: string;
  I: integer;
begin
  if FileExistsUTF8(txtSourceExe.Text) { *Converted from FileExists*  } then
  begin
    try
      lstLog.Items.Add('Merging assemblies...Please wait');
      Application.ProcessMessages;
      ilMergeLog := ChangeFileExt(txtSourceExe.Text, '.log');

      if FileExistsUTF8(ilMergeLog) { *Converted from FileExists*  } then
        DeleteFileUTF8(ilMergeLog); { *Converted from DeleteFile*  }


      case AppType of
        maUnknown: ;
        maEXE: mergeType := 'winexe';
        maDLL: mergeType := 'Library';
      end;

      for I := 0 to lstOtherAssemblies.Items.Count - 1 do
        assemblies := assemblies + ' "' + lstOtherAssemblies.Items[I] + '"';

      if chkAdditionalCommands.Checked then
        ilMergerParams := txtAdditionalCommands.Text;

      ilMergerParams := ilMergerParams + '/wildcards /log:"' + ilMergeLog +
        '" /t:' + mergeType + ' /out:"' + txtMergedExe.Text + '" "' +
        txtSourceExe.Text + '" ' + assemblies;

      //ShellExecute_AndWait(ilMergePath,ilMergerParams);
      IlMergeProcess.ApplicationName := ILMergePath;
      IlMergeProcess.CommandLine := ilMergerParams;
      IlMergeProcess.ShowWindow := swoHIDE;

      IlMergeProcess.Execute;

      lstLog.Items.Clear;

      lstLog.Items.Add('Executing merge...');
      lstLog.Items.Add('*');
      ;
      while IlMergeProcess.Active do
      begin
        lstLog.Items[lstLog.Items.Count-1] := lstLog.Items[lstLog.Items.Count-1] + '*';
        Application.ProcessMessages;
      end;

      lstLog.Items.LoadFromFile(ilMergeLog);
      lstLog.ItemIndex := lstLog.Items.Count - 1;

    except
      on e: Exception do
        MessageDlg(e.Message, mtError, [mbOK], 0)
    end;
  end
  else
  begin
    MessageDlg('Specified source assembly, does not exist', mtError, [mbOK], 0);
  end;
end;

procedure TMainForm.ExecuteMpress;
var
  mpressPath:string;
  params:string;
  r:TResourceStream;
  fs:TFileStream;
  beforeFileSize:Integer;
  afterFileSize:Integer;
  filename:string;
begin
  mpressPath := GetTempFileName(GetTempDir,'mprs');

  if chkCompressOnly.Checked then
  filename := txtSourceExe.Text
  else
  filename := txtMergedExe.Text;


  try
    r := TResourceStream.Create(HINSTANCE,'MPRESS',RT_RCDATA);
    r.SaveToFile(mpressPath);
  finally
    r.Free;
  end;

  params:=EmptyStr;
  //mpressPath:= ExtractFileDir(Application.ExeName)+'\mpress.exe';
  try
    fs := TFileStream.Create(filename,fmOpenRead or fmShareDenyNone);
    beforeFileSize:= fs.Size;
  finally
    fs.Free;
  end;

  MpressProcess.ApplicationName:= mpressPath;
  MpressProcess.CurrentDirectory := ExtractFilePath(Application.ExeName);
  MpressProcess.ShowWindow:=swoHIDE;


  if chkCreateBackupFile.Checked then
  begin
    CopyFile(txtMergedExe.Text,ChangeFileExt(filename,'.backup'));
  end;

  if chkDontPatchStrongName.Checked  then params:=params+'-S ';
  if chkForceCompression.Checked then params:=params+'-m ';
  if chkDontCompressResources.Checked then params:=params+'-r ';

  MpressProcess.CommandLine := params+'-s "'+filename+'"';
  MpressProcess.Execute;

  lstLog.Items.Add('Starting process...');
  lstLog.ItemIndex:=lstLog.Count-1;

  while not MpressProcess.Active do
  Application.ProcessMessages;

  lstLog.Items.Add('Compressing...');
  lstLog.ItemIndex:=lstLog.Count-1;

  while MpressProcess.Active do
  Application.ProcessMessages;

  lstLog.Items.Add('Done');
  lstLog.ItemIndex:=lstLog.Count-1;
  DeleteFile(mpressPath);

  Sleep(500);

  try
    fs := TFileStream.Create(filename,fmOpenRead or fmShareDenyNone);
    afterFileSize := fs.Size;
  finally
    fs.Free;
  end;

  lstLog.Items.Add('==========');
  lstLog.ItemIndex:=lstLog.Count-1;
  lstLog.Items.Add('Before:'+IntToStr(beforeFileSize div 1024) +'kb');
  lstLog.ItemIndex:=lstLog.Count-1;
  lstLog.Items.Add('After:'+IntToStr(afterFileSize div 1024) +'kb');
  lstLog.ItemIndex:=lstLog.Count-1;
  lstLog.Items.Add('Compression: '+IntToStr((beforeFileSize-afterFileSize) div 1024)+'kb');
  lstLog.ItemIndex:=lstLog.Count-1;

end;

procedure TMainForm.FormCreate(Sender: TObject);
var
  r: TResourceStream;
begin
  il1.GetBitmap(0, btnOpen.Glyph);
  il1.GetBitmap(0, btnAddAssembliess.Glyph);
  il1.GetBitmap(1, btnMergedExe.Glyph);
  il1.GetBitmap(2, btnAbout.Glyph);
  il1.GetBitmap(2,btnAdditionalCommands.Glyph);
  il1.GetBitmap(3, btnMerge.Glyph);
  il1.GetBitmap(4, btnClose.Glyph);

  Application.Title := ApplicationName;
  Self.Caption := ApplicationName + ' ' + ApplicationVersion;
  DragAcceptFiles(Handle, True);
  ILMergePath := GetEnvironmentVariable('TEMP') + '\ILMerge.exe';
  try
    r := TResourceStream.Create(HInstance, 'ILMERGE', RT_RCDATA);
    r.SaveToFile(ILMergePath);
  except
    MessageDlg('Could not create ILMerge in temp directory!', mtError, [mbOK], 0);
    Application.Terminate;
  end;

end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  DragAcceptFiles(Handle, False);

  if FileExists(ILMergePath) then
    DeleteFile(ILMergePath);

end;

procedure TMainForm.FormPaint(Sender: TObject);
begin
  Canvas.GradientFill(ClientRect, clBtnFace, clWhite, gdHorizontal);
end;

procedure TMainForm.pbTitlePaint(Sender: TObject);
begin
  pbTitle.Canvas.GradientFill(pbTitle.ClientRect, clWhite, clSilver, gdVertical);
  with pbTitle.Canvas do
  begin
    Brush.Style := bsClear;
    Font.Size := 14;
    Font.Color := clSilver;
    TextOut(5, 5, ApplicationName + ' ' + ApplicationVersion);
    Font.Color := clBlack;
    TextOut(3, 3, ApplicationName + ' ' + ApplicationVersion);
  end;

end;

procedure TMainForm.Remve1Click(Sender: TObject);
begin
  lstOtherAssemblies.Items.Delete(lstOtherAssemblies.ItemIndex);
end;

procedure TMainForm.WMDropFiles(var Message: TWMDropFiles);
var
  c: integer;
  fname: array [0..255] of char;
begin
  c := DragQueryFile(Message.Drop, $FFFFFFFF, nil, 0);

  if c > 0 then
  begin
    DragQueryFile(Message.Drop, 0, @fname, SizeOf(fname));
    if FileExistsUTF8(fname) { *Converted from FileExists*  } then
    begin
      txtSourceExe.Text := fname;
      SetAppTypeAndOutputName;
      SearchDir(fname);
    end;
  end;

end;

procedure TMainForm.WMNCHitTest(var Message: TWMNCHitTest);
begin
  inherited;
  if Message.Result = HTCLIENT then
    Message.Result := HTCAPTION;
end;

procedure TMainForm.SearchDir(Path: string);
var
  dlls: TStringList;
  I: integer;
begin

  lstOtherAssemblies.Clear;
  try
    dlls := FindAllFiles(ExtractFilePath(Path), '*.dll', False);
    lstOtherAssemblies.Items.AddStrings(dlls);
  finally
    dlls.Free;
  end;
end;

procedure TMainForm.SetAppTypeAndOutputName;
var
  ext: string;
begin
  ext := LowerCase(ExtractFileExt(txtSourceExe.Text));
  if ext = '.exe' then
    AppType := maEXE
  else if ext = '.dll' then
    AppType := maDLL
  else
    AppType := maUnknown;
  if Trim(txtMergedExe.Text) = EmptyStr then
    txtMergedExe.Text := ChangeFileExt(txtSourceExe.Text, '') + '_Merged' + ext;
end;

procedure TMainForm.UpdateGUIControls(Enable: boolean);
begin
  btnOpen.Enabled := Enable;
  btnAddAssembliess.Enabled := Enable;
  btnMergedExe.Enabled := Enable;
  btnAbout.Enabled := Enable;
  btnMerge.Enabled := Enable;
  btnClose.Enabled := Enable;
end;

end.

