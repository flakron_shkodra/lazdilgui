unit DocFormU;

{$MODE Delphi}

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ComCtrls,GraphUtil, MainFormU,Buttons;

type

  { TDocForm }

  TDocForm = class(TForm)
    txtDocumentation: TMemo;
    pbTitle: TPaintBox;
    bvl1: TBevel;
    btnClose: TBitBtn;
    btnMaximize: TBitBtn;
    procedure bvl1ChangeBounds(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure pbTitlePaint(Sender: TObject);
    procedure FormPaint(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnMaximizeClick(Sender: TObject);
  private
  protected
    procedure WMNCHitTest(var Message: TWMNCHitTest); message WM_NCHITTEST;
    { Private declarations }
  public
    { Public declarations }

  end;

const
  DocFormCaption = 'ILMerge Documentation';
var
  DocForm: TDocForm;

implementation

{$R *.lfm}

procedure TDocForm.btnMaximizeClick(Sender: TObject);
begin
  if WindowState = wsNormal then
  WindowState := wsMaximized
  else
  WindowState := wsNormal;
end;

procedure TDocForm.btnCloseClick(Sender: TObject);
begin
 WindowState := wsNormal;
 Close;
end;

procedure TDocForm.FormPaint(Sender: TObject);
begin
  Canvas.GradientFill(Self.ClientRect,clBtnFace,clWhite,gdVertical);
end;

procedure TDocForm.FormShow(Sender: TObject);
begin
end;

procedure TDocForm.pbTitlePaint(Sender: TObject);
begin
  pbTitle.Canvas.GradientFill(pbTitle.ClientRect,clWhite,clSilver,gdVertical);
  with pbTitle.Canvas do
  begin
    Brush.Style := bsClear;
    Font.Size := 14;
    Font.Color := clSilver;
    TextOut(5,5,DocFormCaption);
    Font.Color := clBlack;
    TextOut(3,3,DocFormCaption);
  end;

end;

procedure TDocForm.FormCreate(Sender: TObject);
begin
 MainForm.il1.GetBitmap(4,btnClose.Glyph);
 MainForm.il1.GetBitmap(5,btnMaximize.Glyph);
end;

procedure TDocForm.bvl1ChangeBounds(Sender: TObject);
begin

end;

procedure TDocForm.WMNCHitTest(var Message: TWMNCHitTest);
begin
  inherited;

  if Message.Result = HTCLIENT then
  Message.Result := HTCAPTION;

end;

end.
